package pageLocators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AutomationPracticeHomeContent {
	@FindBy (xpath = "//a[contains(text(), 'Women')]") 
	public WebElement womenLink;
	
	
	@FindBy (xpath = "/html/body/div/div[1]/header/div[2]/div/div/nav/div[2]/a") 
	public WebElement logOut;
}
