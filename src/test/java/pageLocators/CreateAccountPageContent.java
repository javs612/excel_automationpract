package pageLocators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CreateAccountPageContent {
	@FindBy (xpath = "//*[@id=\"noSlide\"]/h1")	
	public WebElement createAccountTitle;
	
	@FindBy (xpath = "//*[@id=\"id_gender1\"]")	
	public WebElement mrCheckBox;
	
	@FindBy (xpath = "//*[@id=\"id_gender2\"]")	
	public WebElement mrsCheckBox;
	
	@FindBy (xpath = "//*[@id=\"customer_firstname\"]")	
	public WebElement firstNamePersonalField;
	
	@FindBy (xpath = "//*[@id=\"customer_lastname\"]")	
	public WebElement lasttNamePersonalField;
	
	@FindBy (xpath = "//*[@id=\"email\"]")	
	public WebElement emailField;
	
	@FindBy (xpath = "//*[@id=\"passwd\"]")	
	public WebElement passwordField;
	
	@FindBy (xpath = "//*[@id=\"days\"]")	
	public WebElement daysSelect;
	
	@FindBy (xpath = "//*[@id=\"months\"]")	
	public WebElement monthsSelect;
	
	@FindBy (xpath = "//*[@id=\"years\"]")	
	public WebElement yearsSelect;
	
	@FindBy (xpath = "//*[@id=\"firstname\"]")
	public WebElement firstnameAddressField;
	
	@FindBy (xpath = "//*[@id=\"lastname\"]")
	public WebElement lastnameAddressField;
	
	@FindBy (xpath = "//*[@id=\"company\"]")
	public WebElement companyField;
	
	@FindBy (xpath = "//*[@id=\"address1\"]")
	public WebElement address1Field;
	
	@FindBy (xpath = "//*[@id=\"address2\"]")
	public WebElement address2Field;
	
	@FindBy (xpath = "//*[@id=\"city\"]")
	public WebElement cityField;
	
	@FindBy (xpath = "//*[@id=\"id_state\"]")
	public WebElement stateSelect;

	@FindBy (xpath = "//*[@id=\"postcode\"]")
	public WebElement zipCodeField;
	
	@FindBy (xpath = "//*[@id=\"id_country\"]")
	public WebElement countrySelect;
	
	@FindBy (xpath = "//*[@id=\"other\"]")
	public WebElement otherField;
	
	@FindBy (xpath = "//*[@id=\"phone\"]")
	public WebElement phoneField;
	
	@FindBy (xpath = "//*[@id=\"phone_mobile\"]")
	public WebElement mobilePhoneField;
	
	@FindBy (xpath = "//*[@id=\"alias\"]")
	public WebElement aliasField;
	
	@FindBy (xpath = "//*[@id=\"submitAccount\"]")
	public WebElement submitAccountBtn;
	
}

