package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import pageLocators.CreateAccountPageContent;



public class CreateAccountPage {
	private WebDriver driver;
	public CreateAccountPageContent createAccountPageContent;
	
	public CreateAccountPage(WebDriver webDriver) {
		driver = webDriver;
		createAccountPageContent = PageFactory.initElements(driver, CreateAccountPageContent.class);
	}
	
	public void createAccountFromExcel(String ...data) {
		if (data[0].equalsIgnoreCase("Mr")) {
			createAccountPageContent.mrCheckBox.click();
		}else {
			createAccountPageContent.mrsCheckBox.click();
		}
		createAccountPageContent.firstNamePersonalField.sendKeys(data[1]);
		createAccountPageContent.lasttNamePersonalField.sendKeys(data[2]);
		//createAccountPageContent.emailField.sendKeys(data[3]);
		createAccountPageContent.passwordField.sendKeys(data[4]);
		String[] birthDay = data[5].split("/");
		Select selectDay = new Select(createAccountPageContent.daysSelect);
		selectDay.selectByValue(birthDay[0]);
		Select selectMonth = new Select(createAccountPageContent.monthsSelect);
		selectMonth.selectByValue(birthDay[1]);
		Select selectYear = new Select(createAccountPageContent.yearsSelect);
		selectYear.selectByValue(birthDay[2]);
		//createAccountPageContent.firstnameAddressField.sendKeys(data[6]);
		//createAccountPageContent.lastnameAddressField.sendKeys(data[7]);
		createAccountPageContent.companyField.sendKeys(data[8]);
		createAccountPageContent.address1Field.sendKeys(data[9]);
		createAccountPageContent.address2Field.sendKeys(data[10]);
		createAccountPageContent.cityField.sendKeys(data[11]);
		Select selectState = new Select(createAccountPageContent.stateSelect);
		selectState.selectByVisibleText(data[12]);
		createAccountPageContent.zipCodeField.sendKeys(data[13]);
		Select selectCountry = new Select(createAccountPageContent.countrySelect);
		selectCountry.selectByVisibleText(data[14]);
		createAccountPageContent.otherField.sendKeys(data[15]);
		createAccountPageContent.phoneField.sendKeys(data[16]);
		createAccountPageContent.mobilePhoneField.sendKeys(data[17]);
		createAccountPageContent.aliasField.sendKeys(data[18]);
	}
	
	public void clickRegisterAccountBtn() {
		createAccountPageContent.submitAccountBtn.click();
	}
	
	
}
