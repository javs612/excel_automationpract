package tests;

import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import config.Setup;
import pageObjects.AutomationPracticeHomePage;
import pageObjects.CreateAccountPage;
import pageObjects.SignInPage;
import pageObjects.WomenPage;
import utils.ExcelUtils;

public class AutomationPracticeHomeActions extends Setup{
	
	static Object[][] testObjArray;
	String testCaseWorkBook = System.getProperty("user.dir") + "/resources/WorkBook.xls";

	
	@DataProvider(name = "UserRegistration")
	public Object[][] userRegister() throws Exception{
		testObjArray = ExcelUtils.getTableArray(testCaseWorkBook, "Sheet1");
		return (testObjArray);
	}

	@Test(dataProvider = "UserRegistration", description="Test Case for Register an user")
	public void testCase(String...registerInfo) throws InterruptedException{
		AutomationPracticeHomePage automationPracticeHomePage = new AutomationPracticeHomePage(driver);
		WomenPage wp = new WomenPage(driver);
		CreateAccountPage createaccountpage = new CreateAccountPage(driver);

		SignInPage signIn = new SignInPage(driver);				
		wp.clickAnyLink("Sign in");

		signIn.enterEmail(registerInfo[3]);
		signIn.clickCreateAccountBtn();
		Assert.assertTrue(signIn.veryfyCreateAccountTitle(), "something went wrong");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		createaccountpage.createAccountFromExcel(registerInfo);
		
		createaccountpage.clickRegisterAccountBtn();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//Logout
		automationPracticeHomePage.logOutBtn();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		
	}

}
