package config;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class Setup {
	public static WebDriver driver;
	static String chromePath = System.getProperty("user.dir")+"/drivers/chromedriver.exe";
	
	@BeforeSuite
	public static void setBrowser() {
		System.setProperty("webdriver.chrome.driver", chromePath);
		driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//Navigate espera a que se termine de cargar la página
		driver.navigate().to("http://automationpractice.com/index.php/");
		driver.manage().window().maximize();
	}
	
	@AfterSuite
	public void closeBrowser() {
		driver.close();
	}
}
